# Descripción
Este pipeline hace la función de ETL (Extracción transformación y carga), extrayendo la información de ids de aplicaciones, procesando y enriqueciendolas mediante "scraping" llamando a la API de [Google Play Store](https://pypi.org/project/google-play-scraper/) procesando y recogiendo las características útiles de cara a almacenar dicha información.

Estás se cargarán en una base de datos PostgreSQL, donde se encuentran ya creadas las tablas en las que se almacenarán de forma ordenada dicha información.

# Requisitos
Para poder ejecutar dicho pipeline necesitamos:

## 1. Cuenta de Google Play Store
Crear una cuenta en de [Google Cloud Platform](https://cloud.google.com/), si es la primera vez, existen opciones free con tiempo y créditos limitados.
### 1.1. Proyecto
Se necesita crear un proyecto, y obtener la id de dicho proyecto
### 1.2. Bucket
Se necesita crear un bucket para poder ejecutar este proyecto
### 1.3. Habilitar acceso
Para poder acceder al bucket ya creado, necesitamos tener habilitado nuestra IP (o acceso público) en las redes entrantes dentro de Firewall, más información [aquí](https://cloud.google.com/network-connectivity/docs/vpn/how-to/configuring-firewall-rules#:~:text=Traffic%20containing%20the%20protocols%20UDP%20500%2C%20UDP%204500%2C,Cloud%20VPN%20gateway%20to%20a%20peer%20VPN%20gateway.)
### 1.4. Obtener clave de cuenta de servicio
Para poder acceder desde python al bucket necesitamos crear una cuenta de servicio, más información [aquí](https://support.google.com/a/answer/7378726?hl=es)
es necesario renombrar dicho archivo json a **credentials.json**
### 1.5. Crear el archivo **gcp_config.json**
Necesitamos crear un archivo **gcp_config.json** con la información de nuestro bucket
````json
{"project": " ... ",
 "bucket_name": " ... "}
 ````
## 2. Base de datos PostgreSQL
### 2.1. Creación del servidor
 Crear un servidor de base de datos [PostgreSQL](https://www.postgresql.org/), 
(como opción [Google Cloud Platform](https://cloud.google.com/) da la opción también de crear una base de datos SQL PostgreSQL)
### 2.2. Crear el archivo **db_config.json**
````json 
{"database": " ... ",
 "user": " ... ",
 "password": " ... ",
 "host": "xxx.xxx.xxx.xxx",
 "port": 5432}
 ````
### 2.3. Creación de las tablas
Se provee para ello el siguiente archivo de instrucciones [DLL](https://gitkc.cloud/data-muffin/proyecto-final/-/blob/main/Base%20de%20Datos/ddl.txt)
## 3. Configuración del pipeline
Necesitaremos crear un archivo **ex_config.json** y configurar los parámetros de extracción de información para las apps, este proceso tarda bastante así que, al menos para probar, valores bajos.

````json
{"language": "en",
 "country": "uk",
 "batch": 500,
 "num_batches": 10}
 ````
 
# Instrucciones
## 1. Configuración
### 1.1. Carpeta **Resources**
Para pasar la configuración al pipeline, necesitamos la siguiente estructura:
````
  * Resources
    * credentials.json
    * db_config.json
    * ex_config.json
    * gcp_config.json
````
# Ejecución
## Opción 1 - Docker
* En la carpeta donde está el Dockerfile ejecutar:
````dockerfile
build playstore/etl:latest .
run -rm -v <path_to_resources>:/app/resources playstore/etl:latest
````
## Opción 2 - Poetry
[Poetry](https://python-poetry.org/) es un gestor y empaquetador de dependencias y se puede instalar con ``pip install poetry``

* En la carpeta donde se encuentra el archivo **pyproject.toml** realizar ``poetry install``, esto instalará todas las dependencias necesarias
* Ejecutar
````bash
poetry run python etl_pipeline/creación_del_pipeline.py
````
