# -*- coding: utf-8 -*-

# BIBLIOTECA
# Utilidades
import sys
import numpy as np
import pandas as pd
import re
import json
import datetime
import argparse
import logging
# Barra de progreso
from tqdm import tqdm 
# Propios
from tools.bucket_tools import *
from tools.call_to_play_store_api_tools import *
from tools.database_tools import *

# ============== Transformaciones  ==================

def transform_permissions(df):
  # Conteo de subpermisos dentro de permisos 
  for permission in PERMISSIONS_COLUMNS:
    if permission != 'id_app':
      df.loc[df[permission].isna(), permission] = df.loc[df[permission].isna(), permission].apply(lambda x: [])
      df.loc[:, permission] = df.loc[:, permission].apply(lambda x: len(x))
  return df

  # Manejo de valores NAN
def handle_nan_values(df):
  #La NAs de inAppProductPrice imputaremos con "No"
  df['inAppProductPrice'].fillna("No", inplace = True)
  #adSupported con False
  df['adSupported'].fillna(False, inplace = True)
  #dejamos Nas de video como 0 y donde haya url de video quedara un uno
  df['video'].fillna(0, inplace = True)
  df.loc[df.video!=0, 'video'] = 1
  df['video'] = df['video'].astype(int)
  return df.dropna()

# Extrayendo datos del histograma
def transform_histogram(df, column = 'histogram'):
  df['ratings'] = df[column].apply(lambda x: sum(x))
  df['difHistogram'] = df[column].apply(lambda x: x[0]*-4+x[1]*-2+x[3]*2+x[4]*4)
  return df.drop([column], axis = 1)

# Limpieza de palabra de la variable tamaño
def clean_size(word):
  word = re.sub(',','.', str(word).lower()) #Quitamos las comas por puntos
  number = ''
  metric= ''
  for char in word:
    if char.isdigit() or char==".":
      number += char
    else:
      metric+=char
  if metric == "m":
    number = float(number)
  elif metric == "g":
    number = float(number)*1024
  elif metric == 'k':
    number = float(number)/1024
  else:
    number = None
  return number

# Limpieza de palabra de la variable versión de android
def clean_version(word):
    number = ''
    for char in word:
      if char.isdigit() or char==".":
        number += char
        if len(number) == 3:
          number = float(number)
          break
    if number == "":
      number = None
    return number

# Filtro por tiempo
def filter_by_time(df, months):
  new_df = df.copy()
  df['daysSinceReleased'] = ((df['scraped'] - df['released'])/np.timedelta64(1, 'D'))
  df['monthsSinceReleased'] = ((df['scraped'] - df['released'])/np.timedelta64(1, 'M'))
  new_df = df[df['monthsSinceReleased'] <= months].copy()
  new_df['daysSinceReleased'] = new_df['daysSinceReleased'].astype(int)
  new_df['monthsSinceReleased'] = new_df['monthsSinceReleased'].astype(int)
  return new_df

# Todas las transformaciones
def transform(df):
  # Permissions
  df = transform_permissions(df)

  # Manejo de valores NaNs
  df = handle_nan_values(df)

  # Histrograma complejo a rating y difHistogram (simple)
  df = transform_histogram(df)  

  # Limpieza de valores (string a numérico)
  df['size'] = df['size'].apply(lambda x: clean_size(x))
  df['androidVersion'] = df['androidVersion'].apply(lambda x: clean_version(x))

  # Conteo de screenshots aportado
  df['screenshots'] = df['screenshots'].apply(lambda x: len(x))

  # Fechas y tiempo acumulado
  df['released'] = df['released'].apply(lambda x: pd.to_datetime(x, format = '%b %d, %Y').date()) 
  df['updated'] = df['updated'].apply(lambda x: pd.Timestamp.fromtimestamp(x).date())             
  df['scraped'] = df['released'].apply(lambda x: datetime.datetime.now().date()) 
  df = filter_by_time(df, months=24)

  if len(df) == 0:
    return df

  # ¿Actualizó alguna vez?
  df['everUpdated'] = df.apply(lambda x: x['released'] != x['updated'], axis=1)

  return df

# Lee la configuración a partir de archivo
def ex_config():
  db_config_path = './resources/ex_config.json'
  # Leo los datos
  with open(db_config_path) as f:
    data = f.read()
  # Formateo y devuelvo
  return json.loads(data)
  
# ========================== Pipeline ====================================
def pipeline(database, user, password, host, port, batch, max_iterations, 
            lang, country, google_playstore_csv_path):
  
  # --------------------- CONFIG -----------------------------------------------
  # 0.- Conecto a la base de datos
  conn = connect_to_database(database, user, password, host, port)

  # --------------------- INICIO -----------------------------------------------
  # 1.- Si estamos en la primera iteración cargamos ids de kaggle a procesar
  if (len(get_table(conn, 'app_list')) == 0):
    id_apps_from_kaggle = get_id_apps_from_kaggle_file(google_playstore_csv_path)
    set_id_apps_to_process(conn, id_apps_from_kaggle)
  
  # -------------------- PROCESO ITERATIVO -------------------------------------
  for i in range(max_iterations):
    apps = dict()

    # 2.1.- Cojo un batch de ids a procesar (y las modifico a 'en proceso')
    id_apps_to_process = get_batch_of_id_apps_to_process(conn, batch, update=True)

    # 2.2.- Extraigo información de cada apps dentro del lote (proceso lento)
    for id_app in tqdm(id_apps_to_process):  
      apps[id_app] = call_to_api(id_app, lang = 'EN', country = 'UK')

    # 2.3.- Convierto a dataframe
    df = pd.merge(
        pd.DataFrame([app['app'] for app in apps.values() if app != None], columns = COLUMNS_APPS),
        pd.DataFrame([app['permissions'] for app in apps.values() if app != None], columns = PERMISSIONS_COLUMNS),
        left_on='appId', right_on='id_app', how='inner')

    # 2.4.  - Filtro ids
    # 2.4.1.- Por categorías
    df = df.dropna(subset=['genreId'])
    new_id_apps = set(df[df['genreId'].str.contains('GAME')]['appId'])
    df = df.loc[df['appId'].isin(new_id_apps)]

    # 2.5.- Transformaciones
    if len(new_id_apps) > 0:
      df = transform(df)

    # 2.6.- Recopilo información sobre las ids
    new_id_apps = set(df['appId'])
    new_id_apps_not_valid = set.difference(set(id_apps_to_process), new_id_apps)

    if len(new_id_apps) > 0:
      # 2.7.  - Introduzco la información nueva en la base de datos
      # set_autoincrement(conn)
      # 2.7.1.- tabla: app_list
      if len(new_id_apps_not_valid) > 0:
        update_app_list(conn, new_id_apps_not_valid, status=1, commit=True)

      # 2.7.2.- tabla: permissions
      update_permissions_app(conn, df, commit=False)

      # 2.7.3.- tabla: date_release_scrape
      update_date_release_scrape(conn, df, commit=False)

      # 2.7.4.- tabla: info_app
      update_info_app(conn, df, commit=False)

      # 2.7.5.- tabla: calculated_fields
      update_calculated_fields(conn, df, commit=False)

      # 2.7.6.- tabla: image_app
      update_image_app(conn, df, commit=False)

      update_app_list(conn, new_id_apps, status=0, commit=True)


      # 2.8.- Similares
      similars = set.difference(
          set([id_similar for id_similars in df['similarApps'] if id_similars != None for id_similar in id_similars]), 
          set(get_table(conn, 'app_list')['id_app']))
      # 2.8.1.- Subo similares a procesar
      set_id_apps_to_process(conn, similars, commit=True)
    else:
      update_app_list(conn, new_id_apps_not_valid, status=1, commit=True)

    # 2.9.- guardo 
    cleaning_database(conn)
    conn.commit()
  # ---------------------------------------------------------------------------
  close_connection_to_database(conn)


# ========================== Main ==================================
if __name__ == '__main__':
  logging.getLogger().setLevel(logging.INFO)

  # ============== Manejo de variables de entrada ==================
  # Variables de entrada
  parser = argparse.ArgumentParser(description='Config')
  parser.add_argument('--database', type=str, required=False,
                        help='database')
  parser.add_argument('--user', type=str, required=False,
                        help='user')
  parser.add_argument('--password', type=str, required=False,
                        help='password')
  parser.add_argument('--host', type=str, required=False,
                        help='host')
  parser.add_argument('--port', type=int, required=False,
                        help='porte')
  parser.add_argument('--batch', type=int, required=False,
                        help='batch of ids')
  parser.add_argument('--max_iterations', type=int, required=False,
                        help='number of batch')
  parser.add_argument('--lang', type=str, required=False,
                        help='language')
  parser.add_argument('--country', type=str, required=False,
                        help='market')
  parser.add_argument('--google_playstore_csv_path', type=str, required=False,
                        help='the path to gp.csv on gcp')
  args = parser.parse_args()
  # Configuración - Database
  if args.database:
    database_config = {
      'database': args.database,
      'user': args.user,
      'password': args.password,
      'host': args.host,
      'port': args.port,
    }
  else:
    database_config = db_config()
  # Configuración - batch y max iterations
  if args.batch:
    extraction_config = {
      'batch' : args.batch,
      'max_iterations' : args.max_iterations,
      'lang' : args.lang,
      'country' : args.country
    }
  else:
    extraction_config = ex_config()
  # Archivo de kaggle
  if args.country:
    google_playstore_csv_path = args.google_playstore_csv_path
  else:
    google_playstore_csv_path = 'gs://storage-play-store/Datalake/Kaggle/Google-Playstore.csv'

  print(database_config)
  pipeline(
    **database_config,
    **extraction_config,
    google_playstore_csv_path=google_playstore_csv_path)