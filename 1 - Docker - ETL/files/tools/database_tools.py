# Base de datos
import pandas as pd
import json
import psycopg2
from psycopg2.extras import execute_batch

# Conexión a la base de datos
def connect_to_database(database, user, password, host, port):
  try:
    conn=psycopg2.connect(
        database=database,
        user=user,
        password=password,
        host=host,
        port="{}".format(port)
    )
    return conn
  except:
    print("Error: Conexión con la base de datos no establecida")

# Cerramos conexión
def close_connection_to_database(conn, save=True):
  try:
    if save:
      conn.commit()
    conn.close()
  except:
    print('Warning: Problema al cerrar conexión, posiblemente ya cerrada')
    pass

# Lee los datos de la conexión
def db_config():
  db_config_path = './resources/db_config.json'
  # Leo los datos
  with open(db_config_path) as f:
    data = f.read()
  # Formateo y devuelvo
  return json.loads(data)

# Nombre de tablas
def get_tables_name():
    names = [
        'app_list', 'info_app', 'permissions_app', 'image_app',
        'date_release_scrape', 'calculated_fields']
    return names

# Devuelve tabla
def get_table(conn, table_name):
    if table_name in get_tables_name():
        sql = 'SELECT * FROM {}'.format(table_name)
        return pd.read_sql_query(sql, conn)
    else:
        return None

# Devuelve info de las keys
def get_keys_info(conn, table_name):
    if table_name in get_tables_name():
        cursor = conn.cursor()
        sql =  """
        SELECT con.*
        FROM pg_catalog.pg_constraint con
            INNER JOIN pg_catalog.pg_class rel
                        ON rel.oid = con.conrelid
            INNER JOIN pg_catalog.pg_namespace nsp
                        ON nsp.oid = connamespace
        WHERE rel.relname = '{}';
        """
        return pd.read_sql(sql.format(table_name), conn)
    else:
        return None

# Devuelve tipos de la tabla
def get_types_from_table(conn, table_name):
    if table_name in get_tables_name():
        cursor = conn.cursor()
        sql =  """
        SELECT
            column_name,
            data_type,
            character_maximum_length AS max_length
        FROM
            information_schema.columns
        WHERE
            table_schema = 'public' AND 
            table_name = '{}';
        """
        sql = sql.format(table_name)
        return pd.read_sql(sql, conn)
    else:
        return None

# Setea el autoincremento
def set_autoincrement(conn):
  tables = get_tables_name()
  for table in get_tables_name()[1:]:
    if table != 'date_release_scrape':
      sql = "SELECT setval('{}_id_seq', (select max(id) from {}))".format(table, table)
      cursor = conn.cursor()
      cursor.execute(sql)
    else:
      sql = "SELECT setval('{}_id_seq', (select max(id) from {}))".format('date_realease_scrape', table)
      cursor = conn.cursor()
      cursor.execute(sql)

# Inserta ids en pendientes a procesar
def set_id_apps_to_process(conn, id_apps, commit=True):
    cursor = conn.cursor()

    # Obtenemos las ids que hay en la tabla
    tbl = get_table(conn, 'app_list')
    try:
      id_max = max(tbl.id)
    except:
      id_max = 0
    # Valores id_apps no duplicado
    id_apps = set.difference(set(id_apps), set(tbl.id_app))
    # Definimos la query
    sql = """ INSERT INTO app_list (id, id_app, status) VALUES (%s,%s,%s)"""
    # Ejecutamos
    values = [(i+id_max+1, id_app, 2) for i, id_app in enumerate(id_apps)]
    execute_batch(cursor, sql, values)
    if commit:
        conn.commit()

# Limpia los datos en progreso de id_app
def clean_id_app_in_progress(conn):
  cursor = conn.cursor()
  sql = "UPDATE app_list SET status = 2 WHERE status = 3"
  cursor.execute(sql)

# Devuelve lote de ids a procesar, actualizandolas a "En proceso"
def get_batch_of_id_apps_to_process(conn, batch, update=True):
    cursor = conn.cursor()
    # Querys sql
    sql1 = "SELECT id_app FROM app_list WHERE status = 2 LIMIT {}".format(batch)
    sql2 = "UPDATE app_list SET status = 3 WHERE id_app = %s"
    # Proceso
    try:
        # Select
        cursor.execute(sql1)
        id_apps_to_process = cursor.fetchall()
        if update:
        # Update
            values = [id for id in id_apps_to_process]
            execute_batch(cursor, sql2, values)
            conn.commit()
    except:
        id_apps_to_process = set()
    return {id_app[0] for id_app in id_apps_to_process}

# Actualiza el status de todas las ids (string) pasada por values
def update_app_list(conn, values, status, commit=True):
    cursor = conn.cursor()

    # Querys sql
    sql = "UPDATE app_list SET status = {} WHERE id_app IN %s".format(status)
    
    # Ejecutamos
    values = tuple(values)
    sql = cursor.mogrify(sql, [values])
    cursor.execute(sql)
    if commit:
        conn.commit()

# sube datos a permission_app
def update_permissions_app(conn, df, commit=True):
    # Definimos el nombre de la tabla
    tbl_name = 'permissions_app'

    # Creamos la tabla con sus columnas (vacía)
    tbl = get_table(conn, tbl_name)

    # Buscamos el id_app_list corespondiente de cada app
    cursor = conn.cursor()
    tbl_app = get_table(conn, 'app_list')
    id_app_list = [int(tbl_app[tbl_app.id_app == id_app].id) for id_app in df.appId]
    
    # Subo a la base de datos
    cols = ','.join(list(tbl.drop('id', axis=1).columns))
    values = tuple(zip(
        #ids, 
        id_app_list, df['Microphone'], 
        df['Wi-Fi connection information'], 
        df['Camera'], df['Photos/Media/Files'], df['Storage'], 
        df['Device ID & call information'], df['Contacts'], 
        df['Phone'], df['Identity'], df['Other'], df['Uncategorized'],
        df['Device & app history'], df['Location'], 
        df['Cellular data settings'],
        df['Calendar']))
    sql_sub = ("%s, " * (len(tbl.columns) - 1)).strip().strip(',')
    sql = "INSERT INTO {}({}) VALUES ({})".format(tbl_name, cols, sql_sub)
    execute_batch(cursor, sql, values)
    if commit:
        conn.commit()

# sube datos a date_release_scrape
def update_date_release_scrape(conn, df, commit=True):
    # Definimos el nombre de la tabla
    tbl_name = 'date_release_scrape'

    # Creamos la tabla con sus columnas (vacía)
    tbl = get_table(conn, tbl_name)

    # Buscamos el id_app_list corespondiente de cada app
    cursor = conn.cursor()
    tbl_app = get_table(conn, 'app_list')
    id_app_list = [int(tbl_app[tbl_app.id_app == id_app].id) for id_app in df.appId]

    # Subo a la base de datos
    cols = ','.join(list(tbl.drop('id', axis=1).columns))
    values = tuple(zip(
        id_app_list, df['released'], df['scraped']))
    sql_sub = ("%s, " * (len(tbl.columns)-1)).strip().strip(',')
    sql = "INSERT INTO {}({}) VALUES ({})".format(tbl_name, cols, sql_sub)
    execute_batch(cursor, sql, values)
    if commit:
        conn.commit()

# sube datos a calculated_fields
def update_calculated_fields(conn, df, commit=True):
    # Definimos el nombre de la tabla
    tbl_name = 'calculated_fields'

    # Creamos la tabla con sus columnas (vacía)
    tbl = get_table(conn, tbl_name)

    # Buscamos el id_app_list corespondiente de cada app
    cursor = conn.cursor()
    tbl_app = get_table(conn, 'app_list')
    id_app_list = [int(tbl_app[tbl_app.id_app == id_app].id) for id_app in df.appId]

    # Subo a la base de datos
    cols = ','.join(list(tbl.drop('id', axis=1).columns))
    values = tuple(zip(
        id_app_list, df['ratings'], df['difHistogram'], df['daysSinceReleased'], 
        df['monthsSinceReleased'], df['everUpdated']))
    sql_sub = ("%s, " * (len(tbl.columns)-1)).strip().strip(',')
    sql = "INSERT INTO {}({}) VALUES ({})".format(tbl_name, cols, sql_sub)
    execute_batch(cursor, sql, values)
    if commit:
        conn.commit()

# sube datos a info_app
def update_info_app(conn, df, commit=True):
    # Definimos el nombre de la tabla
    tbl_name = 'info_app'

    # Creamos la tabla con sus columnas (vacía)
    tbl = get_table(conn, tbl_name)

    # Buscamos el id_app_list corespondiente de cada app
    cursor = conn.cursor()
    tbl_app = get_table(conn, 'app_list')
    id_app_list = [int(tbl_app[tbl_app.id_app == id_app].id) for id_app in df.appId]

    # Subo a la base de datos
    cols = ','.join(list(tbl.drop('id', axis=1).columns))
    values = tuple(zip(
        df['minInstalls'], df['reviews'], df['free'], df['offersIAP'], 
        df['adSupported'], id_app_list, df['title'], df['description'], df['summary'],
        df['score'], df['price'], df['inAppProductPrice'], df['size'], df['androidVersion'], 
        df['developer'], df['developerId'], df['genreId'], df['video'], df['contentRating'],
        df['containsAds'], df['similarApps']))
    sql_sub = ("%s, " * (len(tbl.columns)-1)).strip().strip(',')
    sql = "INSERT INTO {}({}) VALUES ({})".format(tbl_name, cols, sql_sub)
    execute_batch(cursor, sql, values)
    if commit:
        conn.commit()

# sube datos a image_app
def update_image_app(conn, df, commit=True):
    # Definimos el nombre de la tabla
    tbl_name = 'image_app'

    # Creamos la tabla con sus columnas (vacía)
    tbl = get_table(conn, tbl_name)

    # Buscamos el id_app_list corespondiente de cada app
    cursor = conn.cursor()
    tbl_app = get_table(conn, 'app_list')
    id_app_list = [int(tbl_app[tbl_app.id_app == id_app].id) for id_app in df.appId]
    
    # Subo a la base de datos
    cols = ','.join(list(tbl.drop('id', axis=1).columns))
    values = tuple(zip(
        id_app_list, df['screenshots'], df['icon'], df['headerImage']))
    sql_sub = ("%s, " * (len(tbl.columns)-1)).strip().strip(',')
    sql = "INSERT INTO {}({}) VALUES ({})".format(tbl_name, cols, sql_sub)
    execute_batch(cursor, sql, values)
    if commit:
        conn.commit()

"""* Delete"""

# Borra todos los datos de las tablas
def delete_all_records_from_tables(conn, app_list_included = True, commit=True):
  sql = []
  for table_name in get_tables_name():
    if table_name!='app_list':
      sql.append("TRUNCATE TABLE {}".format(table_name))
    elif app_list_included:
      cursor = conn.cursor()
      sql.append("TRUNCATE TABLE {} CASCADE".format(table_name))
    else:
      sql.append("UPDATE app_list SET status = 2 WHERE status = 0")
      sql.append("UPDATE app_list SET status = 2 WHERE status = 3")
  for query in sql:
    cursor = conn.cursor()
    cursor.execute(query)
  if commit:
    conn.commit()

# Recoje los verdadero valores validos en union de todas las tablas
def get_real_valid_values(conn):
  cursor = conn.cursor()
  sql = """
    SELECT app_list.id
    FROM app_list 
      INNER JOIN info_app ON info_app.id_app_list = app_list.id
      INNER JOIN permissions_app ON permissions_app.id_app_list = app_list.id
      INNER JOIN image_app ON image_app.id_app_list = app_list.id
      INNER JOIN date_release_scrape ON date_release_scrape.id_app_list = app_list.id
      INNER JOIN calculated_fields ON calculated_fields.id_app_list = app_list.id
    WHERE app_list.status = 0
    """
  cursor.execute(sql)
  return set(id[0] for id in cursor.fetchall())

# Setea el status de las ids que no están en la lista
def set_status_from_ids_where_not_in_list(conn, ids):
  cursor = conn.cursor()
  sql = """
    UPDATE status
    FROM app_list
    WHERE id NOT IN ({})
    """.format(str(ids)[1:-1])
  cursor.execute(sql)

# Borra las ids que no estan en la tablas
def delete_ids_if_not_in_table(conn, ids):
  cursor = conn.cursor()
  for table in get_tables_name():
    sql = """
      DELETE 
      FROM {}
      WHERE id_app_list NOT EXISTS ({})
      """.format(table, str(ids)[1:-1])
  cursor.execute(sql)

# Limpieza de la base de datos por fallos, duplicados, etc
def cleaning_database(conn):

  # Cargamos las ids validas y comunes
  ids_validas = get_real_valid_values(conn)

  # Seteamos a status a procesar las que no estén en ids_validas
  try:
    set_status_from_ids_where_not_in_list(conn, ids_validas)
    conn.commit()
  except:
    conn = connect_to_database(**database_config)
    pass

  # Borramos de las tablas las que no sean ids_validas
  try:
    delete_ids_if_not_in_table(conn, ids_validas)
    conn.commit()
  except:
    conn = connect_to_database(**database_config)
    pass
