# -*- coding: utf-8 -*-

# BIBLIOTECA
# Utilidades
import os
import numpy as np
import pandas as pd
import pickle as pickle
import math
import re
import json
import datetime
import logging
# Barra de progreso
from tqdm import tqdm 

# Autoencoder
import tensorflow as tf
import keras_tuner.tuners as kt
from tensorflow.keras import Model
from tensorflow.keras import Sequential
from tensorflow.keras.layers import Dense, Dropout, Input
from tensorflow.keras.losses import MeanSquaredLogarithmicError

# Sklearn
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import  MinMaxScaler

# Propios
from tools.bucket_tools import *
from tools.call_to_play_store_api_tools import *
from tools.database_tools import *

# ================= Columnas ========================
COLUMNS = {'id_app', 'min_installs', 'title', 'description', 'summary', 'score',
           'ratings', 'reviews', 'in_app_product_price', 'size_app', 'android_version',
           'developer_id', 'genre_id', 'n_screenshots', 'video', 'dif_histogram', 
           'content_rating', 'similar_apps', 'id_app_list'}
PERMISSION_COLUMNS = {'microphone', 'wifi_connect_info', 'camera', 'photos_media_files',
                      'storage_per', 'device_id_call_info', 'contacts', 'phone',
                      'identity_per', 'other', 'uncategorized', 'device_and_app_history',
                      'location', 'cellular_data_setting', 'calendar'}
COLUMNS = set.union(COLUMNS, PERMISSION_COLUMNS)
DATE_COLUMNS = {'date_release', 'date_scrape', 'ever_updated', 'days_since_released', 'months_since_released'}
COLUMNS = set.union(COLUMNS, DATE_COLUMNS)

# ============== Transformaciones  ==================

# Para unir las tablas en 1
def join_tables(tbl):
  # Remove posible duplicate and drop ids
  tbl['app_list'] = tbl['app_list'].drop_duplicates(subset=['id_app'])
  tbl['info_app'] = tbl['info_app'].drop_duplicates(subset=['id_app_list']).drop(columns='id')
  tbl['permissions_app'] = tbl['permissions_app'].drop_duplicates(subset=['id_app_list']).drop(columns='id')
  tbl['image_app'] = tbl['image_app'].drop_duplicates(subset=['id_app_list']).drop(columns='id')
  tbl['date_release_scrape'] = tbl['date_release_scrape'].drop_duplicates(subset=['id_app_list']).drop(columns='id')
  tbl['calculated_fields'] = tbl['calculated_fields'].drop_duplicates(subset=['id_app_list']).drop(columns='id')

  df = pd.merge(tbl['app_list'], tbl['info_app'], left_on='id', right_on='id_app_list', how='inner').drop(columns='id_app_list')
  df = pd.merge(df, tbl['permissions_app'], left_on='id', right_on='id_app_list', how='inner').drop(columns='id_app_list')
  df = pd.merge(df, tbl['image_app'], left_on='id', right_on='id_app_list', how='inner').drop(columns='id_app_list')
  df = pd.merge(df, tbl['date_release_scrape'], left_on='id', right_on='id_app_list', how='inner').drop(columns='id_app_list')
  df = pd.merge(df, tbl['calculated_fields'], left_on='id', right_on='id_app_list', how='inner')
  return df

#Declaramos la función para codificar los datos
def encoder_data(df):
    
    #Declaramos la función para la asignar la media suavizada
    def smooth_average_cod(df, column):
        agg = df.groupby(column)['min_installs'].agg(['count', 'mean'])
        counts = agg['count']
        means = agg['mean']
        weights = round(df.shape[0]*0,15)
        smooth = (counts*means + weights*mean_all)/(counts+weights)
        df[column] = df[column].map(smooth)
        dict_1 = smooth.to_dict()
        dict_1['otros'] = mean_all
        dict_values_variables[column] = dict_1
        return 

    #Creamos la función para limpiar las variables tipo texto
    def transform_text(text):
        try:
            text_clean = re.sub('[%s]' % re.escape(string.punctuation),'', text).split()
        except:
            text_clean="Nada"
        return len(text_clean)
    
    #Aplicamos las funciones declaradas sobre el dataframe
    df.loc[:, 'title'] = df['title'].apply(lambda x: transform_text(x))
    df.loc[:, 'description'] = df['description'].apply(lambda x: transform_text(x))
    df.loc[:, 'summary'] = df['summary'].apply(lambda x: transform_text(x))
    df.loc[:, 'min_installs'] = df['min_installs'].apply(lambda x: math.log10(x+1) )
    df.loc[:, 'reviews'] = df['reviews'].apply(lambda x: math.log10(x+1))
    
    #Creamos un diccionario para guardar las medias calculadas
    dict_values_variables = {}
    #Calculamos la media total
    mean_all = df['min_installs'].mean()

    #Codificamos las variables categoricas mediante una media suavizada
    smooth_average_cod(df, 'in_app_product_price')
    smooth_average_cod(df, 'android_version')
    smooth_average_cod(df, 'developer_id')
    smooth_average_cod(df, 'genre_id')
    smooth_average_cod(df, 'content_rating')

    #Por otro lado vamos crear dos variabls sintéticas los días y los meses desde el lanzamiento
    #también sacaremos los años, meses y dias de 'released' and 'update'
    df['released_year'] = df['date_release'].apply(lambda x: x.year)
    df['released_month'] = df['date_release'].apply(lambda x: x.month)
    df['released_day'] = df['date_release'].apply(lambda x: x.day)

    #Volvemos a codificar las variables released y update en formato unix y aplicamos logartimo en base 10
    df['date_release'] = df['date_release'].apply(lambda x: math.log10(datetime.datetime.strptime(str(x), '%Y-%m-%d').timestamp()))
    df.drop('date_scrape', axis=1, inplace=True)
    df['ever_updated'] = df['ever_updated'].astype(int)

    return df, dict_values_variables

def reduce_zero_data(df, per_100=15):
  data_0 = df[(df['score']==0)|(df['ratings']==0)|(df['reviews']==0)|(df['min_installs']==0)] 
  data_no_0 = df[(df['score']!=0)&(df['ratings']!=0)&(df['reviews']!=0)&(df['min_installs']!=0)] 
  data_0 = data_0.sample(n=int(round(data_no_0.shape[0]*(per_100/100),0)), random_state=42)
  return pd.concat([data_no_0, data_0], axis=0)

def reduce_outliers(df):
  return df[(df['title']<8)& (df['description']<750)&(df['summary']<25)&(df['size_app']<1000)]

def reduction_dimensionality(df):
    
    #Declaramos la función para el escalado
    def scale_datasets(x_train, x_test, scaler):
      scaler = scaler
      x_train_scaled = pd.DataFrame(
          scaler.fit_transform(x_train.values),
          columns=x_train.columns
      )
      x_test_scaled = pd.DataFrame(
          scaler.transform(x_test.values),
          columns = x_test.columns
      )
      return x_train_scaled, x_test_scaled, scaler
    
    #Declaramos para construir los autoencoders
    def model_autoencoder(list_shape, X, loss):

        #Determinamos el model encode
        inputs = Input(shape=X.shape[1])
        encoded0 = Dense(X.shape[1], activation='relu')(inputs)
        encoded1 = Dense(list_shape[0], activation='relu')(encoded0)
        encoded2 = Dense(list_shape[1], activation='relu')(encoded1)
        encoded3 = Dense(list_shape[2], activation='relu')(encoded2)
        encoded4 = Dense(1, activation='relu')(encoded3)

        #Determinamos el model decode
        decoded1 = Dense(list_shape[2], activation='relu')(encoded4)
        decoded2 = Dense(list_shape[1], activation='relu')(decoded1)
        decoded3 = Dense(list_shape[0], activation='relu')(decoded2)
        decoded4 = Dense(X.shape[1], activation=None)(decoded3)
        autoencoder = Model(inputs, decoded4)
        autoencoder.compile(optimizer='adam', loss=loss)
        autoencoder.fit(X,X,
                        epochs=200,
                        batch_size=20,
                        verbose = 0)

        # Encoder
        encoder = Model(inputs, encoded4)
        # Decoder
        decoder = Model(inputs, decoded4)

        return encoder, autoencoder
      
    #Declaramos la función para imputar los valores del autoencoder    
    def encoder_data(df, list_columns, model, name, scaler):
        data_to_predict = df[list_columns].values
        data_to_predict = scaler.transform(data_to_predict)
        data_to_predict = model(data_to_predict)
        df[name]=data_to_predict
        return

    list_permission = list(PERMISSION_COLUMNS)
    list_date = ['past_months_{}'.format(i) for i in range(24)]

    #Nos quedamos con las columnas que nos interesan    
    data_permission = df[list_permission]
    data_temp = df[list_date]

    #Dividimos en train test los subset
    permission_ae_train, permission_ae_test, temp_ae_train, temp_ae_test = train_test_split(data_permission, data_temp, test_size=0.2, random_state=42)
    
    #Escalamos los datos
    permission_ae_train, permission_ae_test, scaler_permission = scale_datasets(permission_ae_train, permission_ae_test, scaler = MinMaxScaler())
    temp_ae_train, temp_ae_test, scaler_temp = scale_datasets(temp_ae_train, temp_ae_test, scaler = MinMaxScaler())
    
    #Creamos y entrenamos los modelos
    list_shape_permission = [len(PERMISSION_COLUMNS),8,2]
    encoder_permission, autoencoder_permission = model_autoencoder(list_shape_permission, X = permission_ae_train, loss = 'mae')

    list_shape_temp = [len(list_date),12,4]
    encoder_temp, autoenconder_temp = model_autoencoder(list_shape_temp, X = temp_ae_train, loss = 'mse')
    
    #Creamos un test y un aviso en caso de no estar satisfecho con los resultdado
    score_permission = autoencoder_permission.evaluate(permission_ae_test, permission_ae_test, verbose = 1)
    score_temp = autoenconder_temp.evaluate(temp_ae_test, temp_ae_test, verbose = 1)
    
    if score_permission > 0.25: 
      print("Error mayor de lo asumible en autoencoder permission")
    elif score_temp > 0.25:
      print("Error mayor de lo asumible en autoencoder temp")
    #En caso de tener un performance correcto, codificamos los datos en el dataframe
    else:
      encoder_data(df, list_permission, encoder_permission, 'permission', scaler_permission)
      encoder_data(df, list_date, encoder_temp, 'temp', scaler_temp)
      df.drop(list_permission, axis = 1, inplace = True)
      df.drop(list_date, axis = 1, inplace = True)

    return df, encoder_permission, encoder_temp

# ============================== Config ==============================
# Lee la configuración a partir de archivo
def get_db_config():
  config_path = './resources/db_config.json'
  # Leo los datos
  with open(config_path) as f:
    data = f.read()
  # Formateo y devuelvo
  return json.loads(data)

# Lee la configuración a partir de archivo
def get_ex_config():
  config_path = './resources/ex_config.json'
  # Leo los datos
  with open(config_path) as f:
    data = f.read()
  # Formateo y devuelvo
  return json.loads(data)

# Lee la configuración a partir de archivo
def get_gcp_config():
  config_path = './resources/gcp_config.json'
  # Leo los datos
  with open(config_path) as f:
    data = f.read()
  # Formateo y devuelvo
  return json.loads(data)
  
# ========================== Pipeline ====================================
def pipeline(**config):

  # Creamos nuestra propia configuración
  db_config = {k:v for k,v in config.items() if k in ['database', 'user', 'password', 'host', 'port']}
  ex_config = {k:v for k,v in config.items() if k in ['language', 'country']}
  gcp_config= {k:v for k,v in config.items() if k in ['project', 'bucket_name']}

  # Conectamos
  conn = connect_to_database(**db_config)
  conn_alchemy = connect_to_database(**db_config, sqlalchemy=True)

  # Extraemos las tablas y las juntamos [Data Warehouse]
  tbl = dict((table, get_table(conn_alchemy, table)) for table in get_tables_name())
  df = join_tables(tbl)

  # Nos quedamos con las columnas que queremos (basado en análisis exploratorio)
  df = df[list(COLUMNS)]
  df_backup = df.copy()

  # Dropeamos posibles valores nan
  df = df.dropna()

  # Codificación de variables
  df, dict_smooth = encoder_data(df)

  # Guardamos el dict_smooth
  save_dict_smooth_into_bucket(dict_smooth, **gcp_config)

  # Eliminamos outliers
  df = reduce_outliers(df)

  # Reducción de valores 0 a un 15 %
  df = reduce_zero_data(df, per_100=15)

  # Extrae información de la competencia
  try:
    with open('df_1.pkl', 'rb') as f:
      df = pickle.load(f)
  except:
    df = get_competency_data(df, df_backup, **ex_config)
    with open('df_1.pkl', 'wb') as f:
      pickle.dump(df, f)
    if os.path.exists('checkpoint1.pkl'):
      os.remove('checkpoint1.pkl')

  # Extrae información de reviews
  try:
    with open('df_2.pkl', 'rb') as f:
      df = pickle.load(f)
  except:
    df = get_reviews_data(df, **ex_config)
    with open('df_2.pkl', 'wb') as f:
      pickle.dump(df, f)
    if os.path.exists('checkpoint2.pkl'):
      os.remove('checkpoint2.pkl')

  # Reducimos dimensionalidad (autoencoder)
  df, encoder_permission, encoder_temp = reduction_dimensionality(df)
  save_models_to_bucket(encoder_permission=encoder_permission, encoder_temp=encoder_temp, **gcp_config)

  # Codificar la MinInstall en las tres opciones
  df['install_label'] = df['min_installs'].apply(lambda x: 0 if x <= (
    df['min_installs'].mean()-0.5*df['min_installs'].std()) else (1 if x <= (
      df['min_installs'].mean()+0.5*df['min_installs'].std()) else 2)) 


  # Modificamos nuestro dataframe con las columnas e indices que queremos
  df.drop(['id_app', 'min_installs'], axis=1).dropna()

  # Subimos al Data Mart
  
  upload_to_database(conn_alchemy, df)

  # Cerramos
  close_connection_to_database(conn, save=True)


# ========================== Main ==================================
if __name__ == '__main__':
  logging.getLogger().setLevel(logging.INFO)

  # Configuración
  database_config = get_db_config()
  extraction_config = get_ex_config()
  gcp_storage_config = get_gcp_config()

  # Pipeline
  pipeline(
    **database_config,
    **extraction_config,
    **gcp_storage_config)