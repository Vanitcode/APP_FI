FROM debian:latest
LABEL version="1.0" author="Data Muffin"

# Arguments
ARG java_version="openjdk-11-jre"


# --------- Installs ---------------------

# Update & Upgrade
RUN apt-get update && apt-get -y update

# Java
RUN apt-get install -y ${java_version} && \ 
    apt-get clean && rm -rf /var/lib/apt/lists/* && \
    export JAVA_HOME="$(dirname $(dirname $(readlink -f $(which java))))"

# Python
RUN apt-get update && apt-get install -y python

# Pip
RUN apt-get update && apt-get install -y pip
RUN pip -q install pip --upgrade

# libpq-dev
RUN apt-get update && apt-get install libpq-dev -y

# Poetry
RUN pip3 install poetry

# ------------- Copy -----------------------
RUN mkdir app
WORKDIR app
RUN mkdir -p ./train_model
COPY ./files/train_model/__init__.py ./train_model/__init__.py
COPY ./files/train_model/main.py ./train_model/main.py

RUN mkdir -p ./resources
VOLUME ./resources

RUN mkdir -p ./tools
COPY ./files/tools/__init__.py ./tools/__init__.py
COPY ./files/tools/bucket_tools.py ./tools/bucket_tools.py
COPY ./files/tools/database_tools.py ./tools/database_tools.py
COPY ./files/pyproject.toml ./pyproject.toml

# -------------- Poetry --------------------
RUN poetry install --no-interaction --no-ansi

# Runtime
CMD poetry run python ./train_model/main.py