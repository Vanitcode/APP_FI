#Importamos las librerías
import requests
import pandas as pd
import re
import string
import numpy as np
import time
import pickle as pkl
import datetime
from google_play_scraper import app, reviews_all, permissions
from sklearn import preprocessing
from dateutil.relativedelta import relativedelta
import tensorflow as tf
from tensorflow.keras import Model
from sklearn.preprocessing import  MinMaxScaler
import math
import joblib
import os
import gc
#import psycopg2
from sqlalchemy import create_engine
import os
from google.cloud import storage

#Definimos las funciones
def transform_data(appId, project_path, project, bucket_name):
  #Declaramos las constantes
  project_path = project_path
  project = project
  bucket_name = bucket_name

  #Declaramos las funciones
  def transform_text(text):
    try:
        text_clean = re.sub('[%s]' % re.escape(string.punctuation),'', text).split()
    except:
        text_clean="Nada"
    return len(text_clean)
    
  def cod_histogram(value):
    dif_histogram = value[0]*-4+value[1]*-2+value[3]*2+value[4]*4
    return dif_histogram

  def clean_version(word):
      number = ''
      for char in word:
        if char.isdigit() or char==".":
          number += char
          if len(number) == 3:
            number = float(number)
            break

      if number == "":
        number = None
      return number

  def clean_size(word):
    word = str(word)
    word = word.lower()
    word = re.sub(',','.', word)
    number = ''
    metric= ''
    for char in word:
      if char.isdigit() or char==".":
        number += char
      else:
        metric+=char

    if metric == "m":
      number = float(number)
    elif metric == "g":
      number = float(number)*1024
    elif metric == 'k':
      number = float(number)/1024
    else:
      number = None

    return number
  
  def count_permission(appId):
    permission = pd.DataFrame(columns =['microphone', 'wifi_connect_info', 'camera', 'photos_media_files',
                      'storage_per', 'device_id_call_info', 'contacts', 'phone',
                      'identity_per', 'other', 'uncategorized', 'device_and_app_history',
                      'location', 'cellular_data_setting', 'calendar'])
    df =permissions(appId, lang='en', country='uk')
    
    try:
      permission['microphone'] = [len(df['Microphone'])]
    except:
      permission['microphone'] = [0]

    try:
      permission['wifi_connect_info'] = [len(df['Wi-Fi connection information'])]
    except:
      permission['wifi_connect_info'] = [0]

    try:
      permission['camera'] = [len(df['Camera'])]
    except:
      permission['camera'] = [0]

    try:
      permission['photos_media_files'] = [len(df['Photos/Media/Files'])]
    except:
      permission['photos_media_files'] = [0]

    try:
      permission['storage_per'] = [len(df['Storage'])]
    except:
      permission['storage_per'] = [0]

    try:
      permission['device_id_call_info'] = [len(df['Device ID & call information'])]
    except:
      permission['device_id_call_info'] = [0]

    try:
      permission['contacts'] = [len(df['Contacts'])]
    except:
      permission['contacts'] = [0]

    try:
      permission['phone'] = [len(df['Phone'])]
    except:
      permission['phone'] = [0]

    try:
      permission['identity_per'] = [len(df['Identity'])]
    except:
      permission['identity_per'] = [0]

    try:
      permission['other'] = [len(df['Other'])]
    except:
      permission['other'] = [0]

    try:
      permission['uncategorized'] = [len(df['Uncategorized'])]
    except:
      permission['uncategorized'] = [0]

    try:
      permission['device_and_app_history'] = [len(df['Device & app history'])]
    except:
      permission['device_and_app_history'] = [0]

    try:
      permission['location'] = [len(df['Location'])]
    except:
      permission['location'] = [0]

    try:
      permission['cellular_data_setting'] = [len(df['Cellular data settings'])]
    except:
      permission['cellular_data_setting'] = [0]

    try:
      permission['calendar'] = [len(df['Calendar'])]
    except:
      permission['calendar'] = [0]

    value = permission.values

    return value

  def load_dict_smooth(project, bucket_name, project_path):

    # Credenciales
    os.environ [ 'GOOGLE_APPLICATION_CREDENTIALS'] = project_path

    # bucket
    storage_client = storage.Client()
    bucket = storage_client.bucket(bucket_name)

    # Blob
    blob = bucket.blob('Preprocess/dict_smooth.pkl')
    dict_smooth = pkl.loads(blob.download_as_string())


    return dict_smooth
  
  def load_encoder_permission(project, bucket_name, project_path):

    # Credenciales
    os.environ [ 'GOOGLE_APPLICATION_CREDENTIALS'] = project_path

    # bucket
    storage_client = storage.Client()
    bucket = storage_client.bucket(bucket_name)

    # Blob
    blob = bucket.blob('Preprocess/encoder_permission.pkl')
    encoder_permission = pkl.loads(blob.download_as_string())


    return encoder_permission

  def load_encoder_temp(project, bucket_name, project_path):

    # Credenciales
    os.environ [ 'GOOGLE_APPLICATION_CREDENTIALS'] = project_path

    # bucket
    storage_client = storage.Client()
    bucket = storage_client.bucket(bucket_name)

    # Blob
    blob = bucket.blob('Preprocess/encoder_temp.pkl')
    encoder_temp = pkl.loads(blob.download_as_string())

    return encoder_temp
      #Declaramos la función para consultar la información en el dataframe o llamar a la API
  def transform_similar_apps(string_app):
    dictionary = {}
    score = []
    mininstall = []
    reviews = []
    for value in string_app:
      try:
        information = app(value, lang = 'en', country='uk')
        mininstall.append(float(information['minInstalls']))
        score.append(float(information['score']))
        reviews.append(float(information['reviews']))

      except:
        mininstall.append(0)
        score.append(0)
        reviews.append(0)

    dictionary['mean_mininstall'] = sum(mininstall)/len(mininstall)
    dictionary['mean_score'] = sum(score)/len(score)
    dictionary['mean_reviews'] = sum(reviews)/len(reviews)
    return dictionary

  #Declaramos la función para la imputación compleja de las descargar al mes a partir de las reviews
  def temporal_data(appId):
    dictionary = {}

    try:
      data_reviews = pd.DataFrame(reviews_all(appId, lang = 'en', country = 'uk'))
      data_reviews['year'] = data_reviews['at'].apply(lambda x: x.year)
      data_reviews['month'] = data_reviews['at'].apply(lambda x: x.month)

      n = 0
      for i in range(-23,1):
          date = datetime.datetime.now() + relativedelta(months=i)
          year = date.year
          month = date.month
          dictionary['Past_month_'+str(i*-1)] = data_reviews[(data_reviews['year']==year)&(data_reviews['month']==month)].shape[0]/data_reviews.shape[0]
          n += dictionary['Past_month_'+str(i*-1)]

      if n == 0:
          for i in range(-23,1):
              date = datetime.datetime.now() + relativedelta(months=i)
              year = date.year
              month = date.month
              dictionary['Past_month_'+str(i*-1)] = 1/24
      else:
          n = 0


      dictionary['rate_thumbsUpCount_by_review'] = data_reviews['thumbsUpCount'].sum()/data_reviews.shape[0]
      dictionary['rate_reply_by_review']= data_reviews[data_reviews['replyContent'].notnull()].shape[0]/data_reviews.shape[0]
      dictionary['mean_score_comment']=data_reviews['score'].mean()


    except:
      for i in range(-23,1):
          date = datetime.datetime.now() + relativedelta(months=i)
          year = date.year
          month = date.month
          dictionary['Past_month_'+str(i*-1)] = 1/24

      dictionary['rate_thumbsUpCount_by_review'] = 0
      dictionary['rate_reply_by_review']= 0
      dictionary['mean_score_comment']= 0

  #Dictionary guardar
    return dictionary

  #Llamamos a la app
  data_app = app(appId, lang="en", country="uk")
  #Creamos un dataframe vacio con la columnas en el mismo orden
  df = pd.DataFrame(columns = ['title', 'content_rating',  'in_app_product_price', 'n_screenshots', 'dif_histogram', 'size_app', 'description', 'ratings', 
                             'genre_id', 'score', 'date_release','video', 'developer_id', 'days_since_released',	'android_version', 
                             'reviews', 'summary', 'ever_updated', 'id_app', 'released_year', 'released_month', 'released_day',
                             'mean_min_installs_competency', 'mean_score_competency', 'mean_reviews_competency', 'rate_thumbsupcount_by_review',
                             'rate_reply_by_reviews', 'mean_score_comment',
                             'permission','temp'])
  
  #Llamamos a la api
  data_app = {'title': data_app['title'],
            'contentRating': data_app['contentRating'],
            'inAppProductPrice': data_app['inAppProductPrice'],
            'screenshots': data_app['screenshots'],
            'histogram': data_app['histogram'],
            'size': data_app['size'],
            'description': data_app['description'],
            'genreId': data_app['genreId'],
            'minInstalls': data_app['minInstalls'],
            'score': data_app['score'],
            'developerId': data_app['developerId'],
            'released': data_app['released'],
            'updated': data_app['updated'],
            'reviews': data_app['reviews'],
            'summary': data_app['summary'],
            'ratings': data_app['ratings'],
            'video': data_app['video'],
            'androidVersion': data_app['androidVersion'],
            'similarApps': data_app['similarApps']
            }
  
  #Codificamos las variables



  df['title'] = [transform_text(data_app['title'])]
  df['description'] = [transform_text(data_app['description'])]
  df['summary'] = [transform_text(data_app['summary'])]
  df['dif_histogram'] = [cod_histogram(data_app['histogram'])]
  df['ratings'] = [data_app['ratings']]
  df['ratings'].fillna(0, inplace = True)
  df['video'] = [data_app['video']]
  df['video'] = df['video'].apply(lambda x: 0 if pd.isnull(x) else 1)
  df['size_app'] = clean_size(data_app['size'])
  df['android_version'] = clean_version(data_app['androidVersion'])
  df['n_screenshots'] = len(data_app['screenshots'])
  df['min_installs'] = math.log10(data_app['minInstalls']+1)
  df['reviews'] = math.log10(data_app['reviews']+1)
  df['score'] = [data_app['score']]
  df['score'].fillna(0, inplace = True)
  df['id_app'] = [appId]
  
  #Recuperamos el diccionario de las medias suavizadas e imputamos su valor
  dicta_smooth = load_dict_smooth(project, bucket_name, project_path)
  
  try:
    df['content_rating'] = dicta_smooth['content_rating'][data_app['contentRating']]
  except:
    df['content_rating'] = dicta_smooth['content_rating']['otros']
  
  try:
    df['in_app_product_price'] = dicta_smooth['in_app_product_price'][data_app['inAppProductPrice']]
  except:
    df['in_app_product_price'] = dicta_smooth['in_app_product_price']['otros']

  try:
    df['developer_id'] = dicta_smooth['developer_id'][data_app['developerId']]
  except:
    df['developer_id'] = dicta_smooth['developer_id']['otros']

  try:
    df['genre_id'] = dicta_smooth['genre_id'][data_app['genreId']]
  except:
    df['genre_id'] = dicta_smooth['genre_id']['otros']

  try:
    df['android_version'] = dicta_smooth['android_version'][df['android_version'].values]
  except:
    df['android_version'] = dicta_smooth['android_version']['otros']

    



  df['date_release'] = [data_app['released']]
  df['date_release'] = [datetime.datetime.strptime(date, '%b %d, %Y').date() for date in df.iloc[:]['date_release'] if type(date) == type(str())]
  df['released_year'] = df['date_release'].apply(lambda x: x.year)
  df['released_month'] = df['date_release'].apply(lambda x: x.month)
  df['released_day'] = df['date_release'].apply(lambda x: x.day)
  df['days_since_released'] = datetime.datetime.now().date() - df['date_release']
  df['days_since_released'] = df['days_since_released'].apply(lambda x: str(x)[:-14])
  df['days_since_released'] = df['days_since_released'].apply(lambda x: int(x))
  df['updated'] = [data_app['updated']]
  df['updated'] = df['updated'].apply(lambda x: pd.Timestamp.fromtimestamp(x).date())
  df['ever_updated'] = df['ever_updated'] == df['updated']
  df['ever_updated'] = df['ever_updated'].apply(lambda x: 0 if x == True else 1)
  df['date_release'] = df['date_release'].apply(lambda x: math.log10(datetime.datetime.strptime(str(x), '%Y-%m-%d').timestamp()))
  df.drop(['updated'], axis = 1, inplace = True)
  
  #Solicitamos los datos de la competencia
  dict_competency = transform_similar_apps(data_app['similarApps'])
  df['mean_min_installs_competency'] = math.log10(dict_competency['mean_mininstall']+1)
  df['mean_score_competency'] = math.log10(dict_competency['mean_score']+1)
  df['mean_reviews_competency'] = math.log10(dict_competency['mean_reviews']+1)

  dict_reviews_all = temporal_data(data_app['similarApps'])
  df['rate_thumbsupcount_by_review'] = dict_reviews_all['rate_thumbsUpCount_by_review']
  df['rate_reply_by_reviews'] = dict_reviews_all['rate_reply_by_review']
  df['mean_score_comment'] = dict_reviews_all['mean_score_comment']

  array_temp = []

  for key in dict_reviews_all.keys():
    if key[:-2] == 'Past_month_' or key[:-1] == 'Past_month_':
        array_temp.append(dict_reviews_all[key])
  
  array_temp = array_temp[::-1]

  array_temp = np.asarray(array_temp)

  temp_permission = load_encoder_temp(project, bucket_name, project_path)
  df['temp'] = temp_permission.predict(array_temp.reshape(1, array_temp.shape[0]))

  # Recalizamos el encoder de permission
  encoder_permission = load_encoder_permission(project, bucket_name, project_path)
  df['permission'] = encoder_permission.predict(count_permission(appId))

  df.drop(['id_app', 'min_installs'], axis = 1, inplace = True)

  histogram = data_app['histogram']
  ratings = data_app['ratings']
  minsInstall = data_app['minInstalls']
  reviews = data_app['reviews']
  score = data_app['score']



  return df, histogram, ratings, minsInstall, reviews, score

def get_values(appId):
  #Constante
  project_path = './resources/credentials.json'
  project = 'datamuffin-playstore-345816'
  bucket_name = 'storage-play-store'
  

  #Transformamos
  value, histogram, ratings, minsInstall, reviews, score = transform_data(appId, project_path, project, bucket_name)
  
  #Cargamos el modelo MinMax y escalamos
  gsc_path_scaler = 'gs://storage-play-store/Preprocess/scaler.save'
  scaler = joblib.load(tf.io.gfile.GFile(gsc_path_scaler, 'rb')) 
  value = scaler.transform(value)


  gsc_path_model = 'gs://storage-play-store/Model/rf_final.pkl'
  model = joblib.load(tf.io.gfile.GFile(gsc_path_model, 'rb')) 

  result = model.predict(value)

  values = {'result': result[0],
            'ratings': ratings,
            'minInstall': minsInstall,
            'reviews': reviews,
            'score': score,
            'reviews': reviews,
            'histogram1': histogram[0],
            'histogram2': histogram[1],
            'histogram3': histogram[2],
            'histogram4': histogram[3],
            'histogram5': histogram[4]}

  return values
